from random import randint

name = input("Hi! What is your name? ")

guess = 1

for count in range(5):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess", guess, ":", name, "were you born in", month, "/", year)
    guess += 1
    response = input("yes or no?\n")

    if response == "yes":
        print("I knew it!")
        exit()
    elif count == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")

